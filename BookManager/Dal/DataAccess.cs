﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BookManager.Model;

namespace BookManager.Dal
{
    /// <summary>
    /// This DataAccess represent interface as singleton to write/read data.
    /// </summary>
    class DataAccess
    {
        // Declare singelton attributes
        private static DataAccess instance;
        public static DataAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    // This sigleton instance
                    instance = new DataAccess();
                }
                return instance;
            }
        }

        /// Constructor
        private DataAccess()
        {
        }

        /// <summary>
        /// This function write book collection to file system
        /// </summary>
        /// <param name="bookCollection">
        /// The collection to save.
        /// </param>
        /// <param name="path">
        /// The path where to save.
        /// </param>
        public void SaveFile(BookCollection bookCollection, string path)
        {
            FileStream fs = new FileStream(path, FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            // Serialize
            bf.Serialize(fs, bookCollection);
            fs.Close();
        }

        /// <summary>
        /// This function read from file system and return the book collection
        /// </summary>
        /// <param name="path">
        /// The path to the file.
        /// </param>
        /// <returns>
        /// The loaded book collection.
        /// </returns>
        public BookCollection LoadFile(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            // Deserialize
            BookCollection bookCollection = (BookCollection)bf.Deserialize(fs);
            fs.Close();
            return bookCollection;
        }
    }
}
