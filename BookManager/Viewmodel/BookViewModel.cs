﻿using System;
using BookManager.Model;

namespace BookManager.Viewmodel
{
    /// <summary>
    /// This represent a view-model of an Book object.
    /// Handle access for the UI.
    /// </summary>
    class BookViewModel
    {
        // Declaration
        private Book book
        {
            set; get;
        }

        /// <summary>
        /// Constructor.
        /// Initialisate the book.
        /// </summary>
        public BookViewModel()
        {
            this.book = new Book();
        }

        /// <summary>
        /// Constructor.
        /// Initialisate a specific book.
        /// </summary>
        public BookViewModel(Book book)
        {
            this.book = book;
        }

        // Amber to show release dates.
        // Left amber light. (old)
        public string LeftLights
        {
            get
            {
                if (book.Release == DateTime.Today)
                {
                    return "Green";
                }
                else
                {
                    return "Gray";
                }
            }
        }

        // Amber to show release dates.
        // Center amber light. (medium old)
        public string CenterLights
        {
            get
            {
                if (book.Release < DateTime.Today
                    && book.Release >= DateTime.Today.AddYears(-10))
                {
                    return "Yellow";
                }
                else
                {
                    return "Gray";
                }
            }
        }

        // Amber to show release dates.
        // Right amber light. (new)
        public string RightLights
        {
            get
            {
                if (book.Release < DateTime.Today.AddYears(-10))
                {
                    return "Red";
                }
                else
                {
                    return "Gray";
                }
            }
        }

        // Property section, each property can show errors 
        // that happen in the Book object
        public Book Book
        {
            set
            {
                book = value;
            }
            get
            {
                return book;
            }
        }

        public String Title
        {
            get
            {
                return book.Title;
            }
            set
            {
                book.Title = value;
            }
        }
        public String Author
        {
            get
            {
                return book.Author;
            }
            set
            {
                book.Author = value;
            }
        }
        public DateTime Release
        {
            get
            {
                return book.Release;
            }
            set
            {
                book.Release = value;
            }
        }
        public String Publishing
        {
            get
            {
                return book.Publishing;
            }
            set
            {
                book.Publishing = value;
            }
        }
        public int PageCount
        {
            get
            {
                return book.PageCount;
            }
            set
            {
                book.PageCount = value;
            }
        }
        public string Isbn
        {
            get
            {
                return book.Isbn;
            }
            set
            {
                book.Isbn = value;
            }
        }
        public int Fsk
        {
            get
            {
                return book.Fsk;
            }
            set
            {
                book.Fsk = value;
            }
        }
        public bool Ebook
        {
            get
            {
                return book.EbookAvailable;
            }
            set
            {
                book.EbookAvailable = value;
            }
        }
        public double Price
        {
            get
            {
                return book.Price;
            }
            set
            {
                book.Price = value;
            }
        }
        public String BookGenre
        {
            get
            {
                return book.BookGenre.ToString();
            }
            set
            {
                book.BookGenre = (Book.GenreE)Enum.Parse(typeof(Book.GenreE), value);
            }
        }
        public String BookType
        {
            get
            {
                return book.BookType.ToString();
            }
            set
            {
                book.BookType = (Book.TypeE)Enum.Parse(typeof(Book.TypeE), value);
            }
        }
    }
}
