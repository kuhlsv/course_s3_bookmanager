﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Microsoft.Win32;
using System.Linq;
using BookManager.Model;
using BookManager.Viewmodel.Commands;
using BookManager.Dal;

namespace BookManager.Viewmodel
{
    /// <summary>
    /// This BookCollectionViewModel represent the view-model
    /// for the book-manager.
    /// </summary>
    class BookCollectionViewModel : ObservableCollection<BookViewModel>
    {
        /// Declare attributes
        private BookCollection bookCollection;
        private DataAccess dataAccess;
        private BookWindow bookWindow;
        private bool isSaved;
        private bool syncDisabled;
        private int selectedBook;
        // Collection commands
        private RelayCommand saveCommand;
        private RelayCommand saveAsCommand;
        private RelayCommand loadCommand;
        private RelayCommand newCommand;
        // Book commands
        private RelayCommand newBookCommand;
        private RelayCommand deleteBookCommand;
        // Book-window command
        private RelayCommand createBookCommand;

        /// <summary> 
        /// Constructor
        /// Inistilisate attributes.
        /// </summary>
        public BookCollectionViewModel()
        {
            // Set DataAccess instance 
            this.dataAccess = DataAccess.Instance;
            // Book Collection Relay Commands
            saveCommand = new RelayCommand(this.SaveBC, this.SyncAllowed);
            saveAsCommand = new RelayCommand(this.SaveAsBC, this.SyncAllowed);
            loadCommand = new RelayCommand(this.OpenBC, this.SyncAllowed);
            newCommand = new RelayCommand(this.NewBC, this.SyncAllowed);
            // Book Relay Commands
            newBookCommand = new RelayCommand(NewBook,
                delegate { return true; });
            deleteBookCommand = new RelayCommand(DeleteBook,
                delegate { return true; });
            createBookCommand = new RelayCommand(CreateBook,
                delegate { return true; });
            // Add methods to the delegate
            bookCollection = new BookCollection();
            bookCollection.CollectionChanged += ModelCollectionChanged;
            this.CollectionChanged += ViewModelCollectionChanged;
            // Initialisation
            this.selectedBook = -1;
            this.isSaved = false;
            this.syncDisabled = false;
        }

        /// <summary> 
        /// New book collection
        /// Creates a new collection and clear current.
        /// </summary>
        private void NewBC()
        {
            this.bookCollection.Clear();
            bookCollection = new BookCollection();
            bookCollection.CollectionChanged += ModelCollectionChanged;
            this.isSaved = false;
        }

        /// <summary>
        /// Open book collection
        /// Open a collection from file.
        /// </summary>
        private void OpenBC()
        {
            Stream myStream = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = "BookCollection";
            dlg.DefaultExt = ".file";
            dlg.InitialDirectory = "C:\\";
            dlg.Filter = "File Documents (.file)|*.file";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                try
                {
                    if ((myStream = dlg.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            if (dlg.FileName != "")
                            {
                                myStream.Close();
                                bookCollection.Clear();
                                foreach (Book newItem in
                                    dataAccess.LoadFile(dlg.FileName))
                                {
                                    bookCollection.Add(newItem);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file." +
                                    " Original error: " + ex.Message);
                }
            }
        }

        /// <summary> 
        /// Save book collection
        /// Save current book collection to file system.
        /// </summary>
        private void SaveBC()
        {
            Stream myStream = null;
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "BookCollection";
            dlg.DefaultExt = ".file";
            dlg.InitialDirectory = "C:\\";
            dlg.Filter = "File Documents (.file)|*.file";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                if ((myStream = dlg.OpenFile()) != null)
                {
                    using (myStream)
                    {
                        myStream.Close();
                        dataAccess.SaveFile(bookCollection, dlg.FileName);
                    }
                    this.isSaved = false;
                }
            }
        }

        /// <summary>
        /// Save as book collection
        /// Save current book collection to file system and specify file type.
        /// </summary>
        private void SaveAsBC()
        {
            Stream myStream = null;
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "BookCollection";
            dlg.InitialDirectory = "C:\\";
            dlg.Filter = "All (*.*)|*.*";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                if ((myStream = dlg.OpenFile()) != null)
                {
                    using (myStream)
                    {
                        if (dlg.FileName != "")
                        {
                            myStream.Close();
                            dataAccess.SaveFile(bookCollection, dlg.FileName);
                        }
                        this.isSaved = false;
                    }
                }
            }
        }

        /// <summary> 
        /// New book
        /// New window to create a new book as bookViewModel.
        /// </summary>
        private void NewBook()
        {
            bookWindow = new BookWindow();
            bookWindow.ShowDialog();
        }

        /// <summary> 
        /// Delete book
        /// This method deletes a book from this collection.
        /// </summary>
        private void DeleteBook()
        {
            if (selectedBook != -1)
            {
                RemoveAt(selectedBook);
                selectedBook = -1;
            }
        }

        /// <summary> 
        /// Create book
        /// This method creates a new book and adds the BookViewModel 
        /// to this collection.
        /// </summary>
        private void CreateBook()
        {
            if (bookWindow.tbTitle.Text != "")
            {
                // Initilisate buffers
                int intBuffer = 0;
                double doubleBuffer = 0;
                // New book instance
                BookViewModel newBook = new BookViewModel();
                // Set book items from inputs and convert data
                newBook.Ebook = bookWindow.cbEbook.IsChecked == true ? true : false;
                newBook.Publishing = bookWindow.tbPublishing.Text;
                newBook.Title = bookWindow.tbTitle.Text;
                newBook.Author = bookWindow.tbAuthor.Text;
                newBook.Isbn = bookWindow.tbIsbn.Text;
                // Release date handling
                try
                {
                    newBook.Release = bookWindow.dpRelease.SelectedDate.Value;
                }
                catch (Exception e)
                {
                    Console.WriteLine(bookWindow.dpRelease.Text);
                }
                // Numeric probertys handling
                // Pages
                if (Int32.TryParse(bookWindow.tbPageCount.Text, out intBuffer))
                {
                    newBook.PageCount = Int32.Parse(bookWindow.tbPageCount.Text);
                }
                // Fsk
                if (Int32.TryParse(bookWindow.tbFsk.Text, out intBuffer))
                {
                    newBook.Fsk = Int32.Parse(bookWindow.tbFsk.Text);
                }
                // Price
                if (Double.TryParse(bookWindow.tbPrice.Text,
                    out doubleBuffer))
                {
                    newBook.Price = Double.Parse(bookWindow.tbPrice.Text);
                }
                else
                {
                    if (Double.TryParse(bookWindow.tbPrice.Text.Replace(
                            ".", ","), out doubleBuffer))
                    {
                        newBook.Price = Double.Parse(
                            bookWindow.tbPrice.Text.Replace(".", ","));
                    }
                }
                // Enum handling (converting them with helper function)
                try
                {
                    newBook.BookType = bookWindow.cbType.Text;
                }
                catch
                {
                    newBook.BookGenre = "None";
                }
                try
                {
                    newBook.BookGenre = bookWindow.cbGenre.Text;
                }
                catch
                {
                    newBook.BookGenre = "None";
                }
                // Add
                this.Add(newBook);
                bookWindow.Close();
            }
        }

        /// <summary> 
        /// View model collection change
        /// This method synchronizes the changes in this collection with the object.
        /// </summary>
        private void ViewModelCollectionChanged(
            object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!syncDisabled)
            {
                syncDisabled = true;
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:

                        foreach (BookViewModel newItem in
                            e.NewItems.OfType<BookViewModel>())
                        {
                            bookCollection.Add(newItem.Book);
                        }
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (BookViewModel oldItem in
                            e.OldItems.OfType<BookViewModel>())
                        {
                            bookCollection.Remove(oldItem.Book);
                        }
                        break;
                }
            }
            syncDisabled = false;
        }

        /// <summary> 
        /// Model collection change
        /// This method synchronizes the changes in the object with this collection.
        /// </summary>
        private void ModelCollectionChanged(
            object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!syncDisabled)
            {
                syncDisabled = true;
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (Book item in e.NewItems.OfType<Book>())
                        {
                            Add(new BookViewModel(item));
                        }
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (Book item in e.OldItems.OfType<Book>())
                        {
                            foreach (BookViewModel existItem in this)
                            {
                                if (existItem.Equals(item))
                                {
                                    Remove(existItem);
                                }
                            }
                        }
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        Clear();
                        break;
                }
            }
            syncDisabled = false;
        }

        /// <summary> 
        /// Relay command to save  book collection.
        /// Read-Only.
        /// </summary>
        public RelayCommand SaveClick
        {
            get
            {
                return saveCommand;
            }
        }

        /// <summary> 
        /// Relay command to save book collection as specific file type.
        /// Read-Only.
        /// </summary>
        public RelayCommand SaveAsClick
        {
            get
            {
                return saveAsCommand;
            }
        }

        /// <summary> 
        /// Relay command to load book collection.
        /// Read-Only.
        /// </summary>
        public RelayCommand LoadClick
        {
            get
            {
                return loadCommand;
            }
        }

        /// <summary>  
        /// Relay command to create new book collection.
        /// Read-Only.
        /// </summary>
        public RelayCommand NewClick
        {
            get
            {
                return newCommand;
            }
        }

        /// <summary> 
        /// Relay command to create new book.
        /// Read-Only.
        /// </summary>
        public RelayCommand NewBookClick
        {
            get
            {
                return newBookCommand;
            }
        }

        /// <summary> 
        /// Relay command to delete a book.
        /// Read-Only.
        /// </summary>
        public RelayCommand DeleteBookClick
        {
            get
            {
                return deleteBookCommand;
            }
        }

        /// <summary> 
        /// Relay command to create a new book.
        /// Read-Only.
        /// </summary>
        public RelayCommand CreateBookClick
        {
            get
            {
                return createBookCommand;
            }
        }

        /// <summary> 
        /// Public attribute to set selected book from UI.
        /// </summary>
        public int SelectedBook
        {
            get
            {
                return selectedBook;
            }
            set
            {
                selectedBook = value;
            }
        }

        /// <summary> 
        /// Sync for delegate.
        /// Returns if syncronisation is activ.
        /// </summary>
        private bool SyncAllowed()
        {
            return !syncDisabled;
        }

        /// <summary>
        /// Helperfunction to convert from input to Enum value (generic, for all enums).
        /// </summary>
        /// <param name="value">
        /// String that convert to an enum value.
        /// </param>
        private static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
