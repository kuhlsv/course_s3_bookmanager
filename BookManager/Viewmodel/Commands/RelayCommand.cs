﻿using System;
using System.Windows.Input;

namespace BookManager.Viewmodel.Commands
{
    /// <summary>
    /// This relay command represent commands based on delegates.
    /// Implements ICommand interface.
    /// </summary>
    class RelayCommand : ICommand
    {
        // Declaration
        private Action cAction;
        private Func<bool> cAllowed;
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Constructor
        /// Initialisate the command with an action and function.
        /// </summary>
        /// <param name="cAction">
        /// The action.
        /// </param>
        public RelayCommand(Action cAction, Func<bool> cIsAllowed)
        {
            this.cAction = cAction;
            this.cAllowed = cIsAllowed;
        }

        /// <summary>
        /// Function to check execute is allowed.
        /// </summary>
        public bool CanExecute(object parameter)
        {
            return this.cAllowed();
        }

        /// <summary>
        /// Function to execute the command.
        /// </summary>
        public void Execute(object parameter)
        {
            this.cAction();
        }
    }
}
