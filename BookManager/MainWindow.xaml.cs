﻿using System;
using System.Windows;
using BookManager.Model;


namespace BookManager
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            InitializeEnumBoxes();
        }

        /// <summary>
        /// Load the Enum-Values to the combo boxes
        /// </summary>
        private void InitializeEnumBoxes()
        {
            for (int i = 0; i < Enum.GetNames(typeof(Book.TypeE)).Length; i++)
            {
                var s = ((Book.TypeE)i).ToString();
                this.cbType.Items.Add(s);
            }
            for (int j = 0; j < Enum.GetNames(typeof(Book.GenreE)).Length; j++)
            {
                var s = ((Book.GenreE)j).ToString();
                this.cbGenre.Items.Add(s);
            }
        }
    }
}
