﻿#pragma checksum "..\..\BookWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "D458A13A884F192672F6A9AA66F28EB26C893D20"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace BookManager {
    
    
    /// <summary>
    /// BookWindow
    /// </summary>
    public partial class BookWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 19 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labTitle;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbTitle;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labAuthor;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbAuthor;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labPublishing;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPublishing;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labRealease;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labPageCount;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPageCount;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labIsbn;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbIsbn;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labPrice;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPrice;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labFsk;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbFsk;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labEbook;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labGenre;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labType;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbEbook;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbGenre;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbType;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpRelease;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Close;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\BookWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/BookManager;component/bookwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\BookWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.labTitle = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.tbTitle = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.labAuthor = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.tbAuthor = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.labPublishing = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.tbPublishing = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.labRealease = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.labPageCount = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.tbPageCount = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.labIsbn = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.tbIsbn = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.labPrice = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.tbPrice = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.labFsk = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.tbFsk = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.labEbook = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.labGenre = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.labType = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.cbEbook = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 20:
            this.cbGenre = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 21:
            this.cbType = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 22:
            this.dpRelease = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 23:
            this.Close = ((System.Windows.Controls.Button)(target));
            return;
            case 24:
            this.Add = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

