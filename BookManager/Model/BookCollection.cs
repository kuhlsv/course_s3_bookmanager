﻿using System;
using System.Collections.ObjectModel;

namespace BookManager.Model
{
    /// <summary>
    /// This is a OberservalbleCollection for the class Book.
    /// It can represent n-amount of books.
    /// </summary>
    [Serializable]
    class BookCollection : ObservableCollection<Book>
    {
        public BookCollection()
        {
        }
    }
}
