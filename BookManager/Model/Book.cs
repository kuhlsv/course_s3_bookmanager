﻿using System;

namespace BookManager.Model
{
    /// <summary>
    /// This represent a book with simple attributes.
    /// </summary>
    [Serializable]
    class Book
    {
        /// <summary>
        /// This enum represent a book genre.
        /// </summary>
        public enum GenreE
        {
            None = 0,
            Action = 1,
            Comedy = 2,
            Drama = 3,
            Horror = 4,
            Literary = 5,
            Romance = 6,
            Satire = 7,
            Tragedy = 8,
            Tragicomedy = 9,
            Fantasy = 10,
            Mythology = 11,
            Biography = 12
        }

        /// <summary>
        /// This enum represent a book type.
        /// </summary>
        public enum TypeE
        {
            None = 0,
            Paperback = 1,
            Hardback = 2,
            Staple = 3,
            Coil = 4
        }
        // Declaration
        private String title;
        private String author;
        private DateTime release;
        private String publishing;
        private int pageCount;
        private string isbn;
        private int fsk;
        private bool ebookAvailable;
        private double price;
        private TypeE bookType;
        private GenreE bookGenre;

        /// Constructor
        public Book()
        {
        }

        /// <summary> 
        /// Constructor.
        /// Inistilisate attributes.
        /// </summary>
        /// <params>
        /// Attributes of the book.
        /// </params>
        public Book(string title, string author, DateTime release,
            string publishing, int pageCount, string isbn, int fsk,
            bool ebookAvailable, double price, GenreE bookGenre, TypeE bookType)
        {
            this.Title = title;
            this.Author = author;
            this.Release = release;
            this.Publishing = publishing;
            this.PageCount = pageCount;
            this.Isbn = isbn;
            this.Fsk = fsk;
            this.EbookAvailable = ebookAvailable;
            this.Price = price;
            this.BookGenre = bookGenre;
            this.BookType = bookType;
        }

        public String Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }
        public String Author
        {
            get
            {
                return author;
            }
            set
            {
                author = value;
            }
        }
        public DateTime Release
        {
            get
            {
                return release;
            }
            set
            {
                release = value;
            }
        }
        public String Publishing
        {
            get
            {
                return publishing;
            }
            set
            {
                publishing = value;
            }
        }
        public int PageCount
        {
            get
            {
                return pageCount;
            }
            set
            {
                pageCount = value;
            }
        }
        public string Isbn
        {
            get
            {
                return isbn;
            }
            set
            {
                isbn = value;
            }
        }
        public int Fsk
        {
            get
            {
                return fsk;
            }
            set
            {
                fsk = value;
            }
        }
        public bool EbookAvailable
        {
            get
            {
                return ebookAvailable;
            }
            set
            {
                ebookAvailable = value;
            }
        }
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
        public TypeE BookType
        {
            get
            {
                return bookType;
            }
            set
            {
                bookType = value;
            }
        }
        public GenreE BookGenre
        {
            get
            {
                return bookGenre;
            }
            set
            {
                bookGenre = value;
            }
        }
    }
}
